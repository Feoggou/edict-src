tall
adjective
o) lofty, big, giant, long-legged, lanky, leggy, Brobdingnagian
    e.g. Being tall can make you incredibly self-confident.

o) high, towering, soaring, steep, elevated, lofty
    [opp: small, short, tiny, wee, squat, stumpy, (archaic, dialect) fubsy]
    e.g. a lawn of tall, waving grass

o) (informal) implausible, incredible, far-fetched, (British, informal) steep, exaggerated, absurd, unbelievable, preposterous, embellished, overblown, (informal) cock-and-bull
    [opp: true, plausible, believable, reasonable, accurate, realistic, unexaggerated]
    e.g. a tall story

o) difficult, hard, demanding, unreasonable, exorbitant, well-nigh impossible
    e.g. Financing your studies can be a tall order.



