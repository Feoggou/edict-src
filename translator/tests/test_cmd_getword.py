import unittest
from unittest import mock
from unittest.mock import patch, call
import os
import json

from src.json_save import JsonSaver
from src.content_retrieval import ContentRetrieval

from src import cmd_getword


class GetWordCmdTest(unittest.TestCase):

    def setUp(self):
        self.defs_content = "defs_content_dummy"
        self.learn_content = "learn_content_dummy"
        self.syn_content = "syn_content_dummy"

        self.old_get_def = ContentRetrieval.get_def_content
        self.old_get_syn = ContentRetrieval.get_syn_content

        ContentRetrieval.get_def_content = mock.Mock(return_value=(self.defs_content, self.learn_content))
        ContentRetrieval.get_syn_content = mock.Mock(return_value=self.syn_content)

    def tearDown(self):
        ContentRetrieval.get_def_content = self.old_get_def
        ContentRetrieval.get_syn_content = self.old_get_syn

    def test_get_word_do_defs(self):
        """
        input: string "do"
        expected result: call a "print" function and save a "do.def" and "do.syn" file (with correct params)
                         for the moment it doesn't matter the content of the save, nor what is printed.
        """

        with patch.object(JsonSaver, 'save') as mock_save_defs:
            cmd_getword.get_word("do")

        calls = [call("do.def", self.defs_content), call("do.learn", self.learn_content), call("do.syn", self.syn_content)]
        mock_save_defs.assert_has_calls(calls)

    def test_get_word_tall_defs(self):
        """
        input: string "tall"
        expected result: call a "print" function and save a "tall.def" and "tall.syn" file.
                         for the moment it doesn't matter the content of the save, nor what is printed.
        """

        with patch.object(JsonSaver, 'save') as mock_save_defs:
            cmd_getword.get_word("tall")

        calls = [call("tall.def", self.defs_content), call("tall.learn", self.learn_content), call("tall.syn", self.syn_content)]
        mock_save_defs.assert_has_calls(calls)

    def test_getword_whenWordHasNoSynonyms(self):
        with patch.object(JsonSaver, 'save') as mock_save_defs:
            with patch.object(ContentRetrieval, "get_syn_content") as mock_get_syn:
                mock_get_syn.side_effect = FileNotFoundError()

                cmd_getword.get_word("blase")

        calls = [call("blase.def", self.defs_content), call("blase.learn", self.learn_content)]
        mock_save_defs.assert_has_calls(calls)


class GetWordCmd_UsingRealFetcher_Test(unittest.TestCase):
    do_def_json = None
    do_learn_json = None
    do_syn_json = None

    @classmethod
    def retrieve_json_content(cls, file_path: str):
        with open(file_path) as f:
            return json.load(f)

    @classmethod
    def setUpClass(cls):
        path = os.path.dirname(os.path.abspath(__file__))

        cls.do_def_json = cls.retrieve_json_content(os.path.join(path, "json_files", "expected_do.def"))
        cls.do_learn_json = cls.retrieve_json_content(os.path.join(path, "json_files", "expected_do.learn"))
        cls.do_syn_json = cls.retrieve_json_content(os.path.join(path, "json_files", "expected_do.syn"))

    def test_get_word_do_defs_usingRealFetcher(self):
        """uses  "Real" HtmlFetcher"""

        with patch.object(JsonSaver, 'save') as mock_save_defs:
            cmd_getword.get_word("do")

        calls = [call("do.def", self.do_def_json), call("do.learn", self.do_learn_json), call("do.syn", self.do_syn_json)]
        mock_save_defs.assert_has_calls(calls)


if __name__ == '__main__':
    unittest.main()
