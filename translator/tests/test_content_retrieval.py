import unittest
from unittest import mock
from unittest.mock import patch

from src.content_retrieval import ContentRetrieval
from src.html_fetcher import HtmlFetcher
from src.html_parser import HtmlParser


class ContentRetrievalTest(unittest.TestCase):

    def setUp(self):
        self.html_content = "dummy_html_content"
        self.exp_json = "dummy_value"

        HtmlFetcher.fetch = mock.Mock(return_value=self.html_content)
        HtmlParser.parse = mock.Mock(return_value=self.exp_json)

    def tearDown(self):
        pass

    def test_getContentDef_tall_retrievesContent(self):
        """
        input: word to retrieve
        result: content for tall.
        """
        with patch.object(HtmlParser, 'parse_learn'):
            content_retrieval = ContentRetrieval()
            result, learn_content = content_retrieval.get_def_content("tall")

        HtmlFetcher.fetch.assert_called_once_with(mock.ANY)
        HtmlParser.parse.assert_called_once_with("tall", self.html_content)

        self.assertEqual(self.exp_json, result)

    def test_getContentDef_do_retrievesContent(self):

        with patch.object(HtmlParser, 'parse_learn'):
            content_retrieval = ContentRetrieval()
            result, learn_content = content_retrieval.get_def_content("do")

        self.assertEqual(self.exp_json, result)
        HtmlFetcher.fetch.assert_called_once_with(mock.ANY)

    def test_get_tall_syn(self):
        exp_json = "dummy_value"
        html_content = "dummy_html_content"

        with patch('src.html_fetcher.HtmlFetcher', autospec=True) as MockHtmlFetcher:
            mock_fetch_obj = MockHtmlFetcher.return_value
            mock_fetch_obj.fetch.return_value = html_content

            with patch.object(HtmlParser, 'parse_syn') as mock_parser:
                mock_parser.return_value = exp_json
                content_retrieval = ContentRetrieval()

                result = content_retrieval.get_syn_content("tall")

        self.assertEqual(exp_json, result)
        mock_fetch_obj.fetch.assert_called_once_with("syn")
        MockHtmlFetcher.assert_called_once_with("tall")


if __name__ == '__main__':
    unittest.main()
