import unittest
from unittest.mock import call
from unittest.mock import patch

from src.translator import Translator


class TranslateTest(unittest.TestCase):
    def test_translateAll_callsTranslate_onEach(self):
        translator = Translator()

        with patch.object(Translator, "translate_file") as mock_translate:
            with patch.object(Translator, "get_html_items") as mock_get_items:
                mock_get_items.return_value = ["a.html", "b.html", "c.html", "d.html"]

                translator.translate_all("path")

        translate_calls = [call("a.html"), call("b.html"), call("c.html"), call("d.html")]
        mock_translate.assert_has_calls(translate_calls)


if __name__ == '__main__':
    unittest.main()
