
from src import html_fetcher
from src.html_parser import HtmlParser


class ContentRetrieval:
    @staticmethod
    def fetch(word: str, what: str):
        fetcher = html_fetcher.HtmlFetcher(word)
        return fetcher.fetch(what)

    def get_def_content(self, word: str):
        html_content = self.fetch(word, "def")

        parser = HtmlParser()
        # NOTE: def_content is optional
        def_content = parser.parse(word, html_content)
        # NOTE: learn content is optional
        learn_content = parser.parse_learn(word, html_content)
        # NOTE: webster content is optional
        webster_content = parser.parse_webster(word, html_content)

        return def_content, learn_content, webster_content

    def get_syn_content(self, word: str):
        html_content = self.fetch(word, "syn")

        parser = HtmlParser()
        return parser.parse_syn(word, html_content)
