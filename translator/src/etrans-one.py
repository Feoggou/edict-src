#!/usr/bin/python3

import sys
import os

from src import config
from src import cmd_getword

sys.path.append(config.PROJECT_PATH)

# dirpath = "/home/zenith/PycharmProjects/edict/v2/html_permanent"
dirpath = os.path.join(config.PROJECT_PATH, "../../edict/v2/html_permanent")


cmd_getword.get_word("a-game-not-worth-the-candle")

print("done!")
