from lxml import etree

from src.def_parser import DefParser
from src.learn_parser import LearnParser
from src.webster_parser import WebsterParser


class Parser:
    @staticmethod
    def get_dictionary_etree_items(root):
        dictionaries = root.xpath('//div[@class="homograph-entry"]/'  # 6
                                  'div[@class=" page"]/'
                                  'div[@class="dictionary dictionaries"]/*'
                                  )

        return dictionaries

    @staticmethod
    def create_def_dictionary(root, word: str):
        dictionaries = Parser.get_dictionary_etree_items(root)

        for x in dictionaries:
            assert isinstance(x, etree._Element)

            if x.get("class") == "Collins_Eng_Dict dictionary":
                return DefParser(root, word)

        return None

    @staticmethod
    def create_learn_dictionary(root, word: str):
        dictionaries = Parser.get_dictionary_etree_items(root)

        for x in dictionaries:
            assert isinstance(x, etree._Element)

            if x.get("class") == "Cob_Adv_Brit dictionary":
                return LearnParser(root, word)

        return None

    @staticmethod
    def create_webster_dictionary(root, word: str):
        dictionaries = Parser.get_dictionary_etree_items(root)

        for x in dictionaries:
            assert isinstance(x, etree._Element)

            if x.get("class") == "Large_US_Webster dictionary":
                return WebsterParser(root, word)

        return None
