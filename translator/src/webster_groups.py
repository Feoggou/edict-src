from .webster_parser import WebsterParser


class WebsterJsonGroup:
    def __init__(self, dict_parser: WebsterParser):
        self.dict_parser = dict_parser

    def translate(self) -> dict:
        pass


class ExamplesGroup(WebsterJsonGroup):
    def __init__(self, dict_parser):
        WebsterJsonGroup.__init__(self, dict_parser)
        self.examples = {}

    def build(self):
        examples = self.dict_parser.get_all_examples()
        self.examples = examples

    def translate(self):
        return self.examples


class WebsterWordDefinition(WebsterJsonGroup):
    def __init__(self, dict_parser: WebsterParser, etree_elem, usage_is_categ: bool=False):
        WebsterJsonGroup.__init__(self, dict_parser)
        self.etree_elem = etree_elem
        self.definition = ""
        self.example = ""
        self.category = ""

    def build(self):
        self.definition = self.dict_parser.get_sense_def(self.etree_elem)
        self.category = self.dict_parser.get_sense_categ(self.etree_elem)
        self.example = self.dict_parser.get_sense_example(self.etree_elem)

    def translate(self) -> dict:
        json_children = {"def": self.definition, "mark": "good"}

        if len(self.category):
            json_children["category"] = self.category

        if len(self.example):
            json_children["example"] = self.example

        return json_children


class WebsterGramGroup(WebsterJsonGroup):
    def __init__(self, dict_parser: WebsterParser, etree_elem):
        WebsterJsonGroup.__init__(self, dict_parser)
        self.etree_elem = etree_elem
        self.defs = []

    def build(self):
        senses = self.dict_parser.get_all_sense_items(self.etree_elem)

        for sense in senses:
            definition = WebsterWordDefinition(self.dict_parser, sense)
            definition.build()
            self.defs.append(definition.translate())

    def translate(self) -> dict:
        json_object = {}
        if self.defs is not None:
            json_object["defs"] = self.defs

        return json_object


class WebsterDefGroup(WebsterJsonGroup):
    def __init__(self, dict_parser: WebsterParser):
        WebsterJsonGroup.__init__(self, dict_parser)
        self.gram_group = None

        self.word = dict_parser.get_def_group_text()

    def build(self):
        gram_group = self.dict_parser.get_grammar_group()

        child = WebsterGramGroup(self.dict_parser, gram_group)
        child.build()
        self.gram_group = child.translate()

    def translate(self) -> dict:
        json_obj = {"word": self.word, "gram_group": self.gram_group}

        return json_obj


class MainWebsterDefGroup:
    def __init__(self, dict_parser: WebsterParser):
        self.dict_parser = dict_parser
        self.def_group = None
        self.examples = None

    def build_children(self):
        self.def_group = WebsterDefGroup(self.dict_parser)
        self.def_group.build()

        # NOTE: Examples group is one, even if you have webster & collins in a single file.
        self.examples = ExamplesGroup(self.dict_parser)
        self.examples.build()

    def translate(self):
        json_object = {}
        if self.def_group is not None:
            json_object["def_group"] = self.def_group.translate()
        if self.examples is not None:
            json_object["examples"] = self.examples.translate()

        return json_object


