from src.content_retrieval import ContentRetrieval
from src.json_save import JsonSaver
from src.json_load import JsonLoader
from src import config


def save_if_needed(word: str, kind: str, content):
    if content == None:
        return

    saver = JsonSaver()
    loader = JsonLoader()

    if config.QUICK >= 1:
        if not loader.exists(word, kind):
            saver.save(word, kind, content)

    elif loader.load(word, kind) != content:
        saver.save(word, kind, content)


def get_word(word: str):
    content_retrieval = ContentRetrieval()
    def_content, learn_content, webster_content = content_retrieval.get_def_content(word)

    save_if_needed(word, "def", def_content)
    save_if_needed(word, "learn", learn_content)
    save_if_needed(word, "webster", webster_content)

    try:
        syn_content = content_retrieval.get_syn_content(word)
        save_if_needed(word, "syn", syn_content)

    except FileNotFoundError:
        # print("Word '{}' has no synonyms.".format(word))
        pass
