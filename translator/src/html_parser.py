from lxml import etree

from .learn_groups import LearnDefGroups
from .def_groups import MainDefGroup
from .webster_groups import MainWebsterDefGroup
from .syn_groups import SynDefGroup

from .syn_parser import SynParser

from .parser import Parser


class HtmlParser:
    def __init__(self):
        self.translated_obj = None

    def parse(self, word_name, html_content: str):
        root = etree.HTML(html_content)
        dict_parser = Parser.create_def_dictionary(root, word_name)
        if dict_parser is None:
            return None

        main_def = MainDefGroup(dict_parser)
        main_def.build_children()
        self.translated_obj = main_def.translate()
        return self.translated_obj

    def parse_syn(self, word_name, html_content: str):
        root = etree.HTML(html_content)
        dict_parser = SynParser(root, word_name)

        main_def = SynDefGroup(dict_parser)
        main_def.build()
        self.translated_obj = main_def.translate()
        return self.translated_obj

    def parse_learn(self, word_name, html_content: str):
        root = etree.HTML(html_content)
        dict_parser = Parser.create_learn_dictionary(root, word_name)
        if dict_parser is None:
            return None

        self.def_groups = LearnDefGroups(dict_parser)
        self.def_groups.build()
        return self.def_groups.translate()

    def parse_webster(self, word_name, html_content: str):
        root = etree.HTML(html_content)
        dict_parser = Parser.create_webster_dictionary(root, word_name)
        if dict_parser is None:
            return None

        main_def = MainWebsterDefGroup(dict_parser)
        main_def.build_children()
        self.translated_obj = main_def.translate()
        return self.translated_obj
