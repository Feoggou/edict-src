import os


class Translator:
    def translate_file(self, file_name: str):
        pass

    def get_html_items(self, directory: str):
        return sorted(os.listdir(directory))

    def translate_all(self, path: str):

        file_items = self.get_html_items(path)

        for filename in file_items:
            self.translate_file(filename)

