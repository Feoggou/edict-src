import os
import json

from src import config


class JsonSaver:
    @staticmethod
    def save(word: str, kind: str, content):
        file_name = word + "." + kind
        beginning_letter = word[0]

        full_name = os.path.join(config.JSON_DIR_PATH, kind, beginning_letter, file_name)

        with open(full_name, "w", encoding="utf-8") as f:
            json.dump(content, f, indent=4, sort_keys=True, ensure_ascii=False)

    @staticmethod
    def save_to_string(content: dict):
        return json.dumps(content, indent=4, sort_keys=True, ensure_ascii=False)