import os
import json

from src import config


class JsonLoader:
    @staticmethod
    def exists(word: str, kind: str):
        file_name = word + "." + kind
        beginning_letter = word[0]

        full_path = os.path.join(config.JSON_DIR_PATH, kind, beginning_letter, file_name)

        return os.path.exists(full_path)

    @staticmethod
    def load(word: str, kind: str):
        file_name = word + "." + kind
        beginning_letter = word[0]
        full_path = os.path.join(config.JSON_DIR_PATH, kind, beginning_letter, file_name)

        if not os.path.exists(full_path):
            return None

        with open(full_path, "r", encoding="utf-8") as f:
            content = json.load(f)

        return content

