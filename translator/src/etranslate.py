#!/usr/bin/python3

import sys
import os
import re
import json

from src import config
from src import cmd_getword
from src import json_load

sys.path.append(config.PROJECT_PATH)

# dirpath = "/home/zenith/PycharmProjects/edict/v2/html_permanent"
dirpath = os.path.join(config.PROJECT_PATH, "../../edict/v2/html_permanent")


all_letters = sorted([x for x in os.listdir(dirpath) if re.match("\w\.json", x)])

failed = 0
total = 0

failed_items = []

for letter in all_letters:
    # letter = all_letters[0]
    # letter = "a.json"

    print("failed = ", failed, "; total = ", total)
    print("letter=", letter)

    with open(os.path.join(dirpath, letter), "r") as f:
        text = json.load(f)

        for item in text:
            total = total + 1

            assert item["have"] is True

            html_item_name = item["link"].split("/")[-1]

            if config.QUICK != 2 or not json_load.JsonLoader().exists(html_item_name, "def"):
                try:
                    cmd_getword.get_word(html_item_name)
                except:
                    failed = failed + 1
                    failed_items.append(html_item_name)

            if total % 1000 == 0:
                print('.', end='', flush=True)

    print("")

with open("failed_items.txt", "w") as f:
    f.write("\n".join(failed_items))

print("failed = ", failed, "; total = ", total)
print("done!")
