from lxml import etree
import re

from src.html_item import *


class WebsterParser:
    def __init__(self, root, word_name):
        self.root = root
        self.word_name = word_name

        self.page = self.root.xpath('//div[@class="homograph-entry"]/'                                # 6
                                    'div[@class=" page"]')[0]

        webster = self.root.xpath('//*[@class="Large_US_Webster dictionary"]')  # KEY 9 (definitions)
        # if len(webster) == 0:
        #     return

        assert(len(webster) == 1)
        self.webster_dict = webster[0]

        def_groups = self.webster_dict.xpath('.//*[@class=" dictentry"]')
        assert len(def_groups) == 1

        self.def_group = def_groups[0]

    def get_def_group_text(self):
        h1_items = self.def_group.xpath('.//*[@class="h1_entry"]')
        assert (len(h1_items) == 1)

        # the normal one in the header: always there
        text_list = h1_items[0].xpath('./*[@class=" orth"]/text()')
        text = "".join(text_list)

        return text

    def get_grammar_group(self):
        groups = self.def_group.xpath('.//div[@class="content definitions american"]/*[@class=" hom"]')
        assert len(groups) == 1

        return groups[0]

    @staticmethod
    def get_all_sense_items(gram_group: etree._Element):
        items = gram_group.xpath('./*[@class=" sense"]')
        return items

    @staticmethod
    def get_sense_def(sense_item: etree._Element):
        items = sense_item.xpath('./*[@class=" def"]')
        assert len(items) == 1

        def_item = items[0]

        text = ParentHtmlItem(def_item).read()
        return text

    @staticmethod
    def get_sense_categ(sense_item: etree._Element):
        items = sense_item.xpath('./*[@class=" lbl" and @type]')

        if len(items) == 0:
            return ""

        text_list = [ParentHtmlItem(usage_item).read() for usage_item in items]
        # we may have ' informal ' or the like.
        text = "".join(text_list).strip()

        return text

    @staticmethod
    def get_sense_example(sense_item: etree._Element):
        items = sense_item.xpath('./*[@class=" cit" and @type="example"]')
        assert len(items) <= 1

        if len(items) == 0:
            return ""

        cit_item = items[0]

        examples = cit_item.xpath('./*[@class=" quote"]')
        assert len(examples) == 1

        text = ParentHtmlItem(examples[0]).read()
        text = text.replace("⇒", "")
        text = text.strip()

        return text

    def get_all_examples(self):

        examples_quotes = self.page.xpath('.//div[@class="content examples"]/'
                                          'span[@class=" cit" and @type="example"]/'
                                          'span[@class=" quote"]')
        if len(examples_quotes) == 0:
            return []

        results = []
        for ex_quote in examples_quotes:
            item = ParentHtmlItem(ex_quote)
            text = item.read()

            # each example is found with appended whitespace (' ')
            # so we need to strip unneeded text.
            text = text.strip()

            results.append(text)

        return results